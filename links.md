# Notes

## Videos

- [hpcalc.org](https://www.youtube.com/@hpcalc/videos)
- [Calculator Clique](https://www.youtube.com/@CalculatorClique/videos)
- [Calculator Culture](https://www.youtube.com/@CalculatorCulture/videos)
- [Christophe de Dinechin: DB48X](https://www.youtube.com/playlist?list=PLz1qkflzABy-Cs1R07zGB8A9K5Yjolmlf)
- [Hey Birt!](https://www.youtube.com/@HeyBirt/videos)
- [Logan West](https://www.youtube.com/@west/videos)
- [Numberphile: Calculator Unboxings](https://www.youtube.com/playlist?list=PLt5AfwLFPxWKAINNfxIdYmFVKuk_F_cQq)
- [Roliny Chupetin: HP-50g A Tutorial for Engineering Students](https://www.youtube.com/playlist?list=PL1-PpkqcSWX53vsPaZaFWgvVF26ewosH6)
- [Stephen Mendes: HP Graphing Calculators](https://www.youtube.com/playlist?list=PL2T2hXG9I1AroRx_V-15DisC-BE2zH_Of)
